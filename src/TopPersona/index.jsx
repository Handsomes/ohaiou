import React from 'react';
import ListLink from '../common/ListLink';
import { orderByScore } from '../utils';
import { Button } from '../common/Button';
import Search from '../common/Search.jsx';

class TopPersona extends React.Component {
  state = {
    persona: [],
    value: '',
    isSearch: false,
    isLoading: false,
  }

  componentDidMount() {
    this.getTopPersona();
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  handleSearch = (event) => {
    event.preventDefault();
    this.setState({ isLoading: true })
    fetch(`https://api.jikan.moe/v3/search/character?q=${this.state.value}`)
      .then(response => response.json())
      .then(data => {
        this.setState({
          persona: data.results,
          isSearch: true,
          isLoading: false,
        })
      })
  }

  getTopPersona = () => {
    fetch('https://api.jikan.moe/v3/top/characters/1')
      .then(a => a.json())
      .then((response) => {
        this.setState({
          persona: response.top
        })
      })
  }

  render() {
    const list = this.state.persona.sort(orderByScore);

    return (
      <React.Fragment>
        <h1>
          {
            this.state.isSearch ? 'Personagens' : 'top 50 Personagens'
          }
        </h1>
        <Search label='Personagens' value={this.state.value} handleChange={this.handleChange} handleSearch={this.handleSearch} />
        {
          this.state.isLoading ? 'Loading...' : (
            <ListLink list={list} prefix='character' info='details' />
          )
        }
        <Button label='Home' to='/' />
      </React.Fragment >
    )
  }
}

export default TopPersona;
