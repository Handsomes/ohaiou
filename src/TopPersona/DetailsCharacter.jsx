import React from 'react';
import { Button } from '../common/Button'

function DetailsCharacter({ details: {
  image_url,
  url,
  name,
  nicknames,
  name_kanji,
  about
}
}) {

  return (
    <React.Fragment>
      <img src={image_url} alt={name} />
      <a href={url}><p>Titulo:{name}</p></a>
      <p>Apelidos:{nicknames}</p>
      <p>Nome em Japonês: {name_kanji}</p>
      <p>Sobre: {about}</p>
      <Button label='TopPersona' to='/top-persona' />
    </React.Fragment>
  )
}

export default DetailsCharacter
