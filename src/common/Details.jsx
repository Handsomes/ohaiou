import React from 'react';
import DetailsAnime from '../TopAnimes/DetailsAnime';
import DetailsCharacter from '../TopPersona/DetailsCharacter';
import DetailsManga from '../TopMangas/DetailsManga';

class Details extends React.Component {
  state = {
    details: {}
  }

  componentDidMount() {
    this.getDetails();
  }

  getDetails = () => {
    fetch(`https://api.jikan.moe/v3/${this.props.match.params.prefix}/${this.props.match.params.id}`, {
      method: 'GET'
    })
      .then(response => response.json())
      .then((data) => {
        this.setState({
          details: data
        });
      })
  }

  render() {
    const { prefix } = this.props.match.params
    return (
      <React.Fragment>
        {prefix === 'anime' ? <DetailsAnime details={this.state.details} /> : null}
        {prefix === 'manga' ? <DetailsManga details={this.state.details} /> : null}
        {prefix === 'character' ? <DetailsCharacter details={this.state.details} /> : null}
      </React.Fragment>
    )
  }
}

export default Details
