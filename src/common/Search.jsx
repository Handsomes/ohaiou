import React from 'react';

function Search({ label, handleChange, value, handleSearch }) {
  return (
    <form onSubmit={handleSearch}>
      <h2>Search for {label}</h2>
      <input value={value} onChange={handleChange} />
      <button type='button' onClick={handleSearch} >GO</button>
    </form>
  )
}

export default Search;
