import React from 'react';
import { Link } from "react-router-dom";

export function Button({ label, to }) {
  return <Link to={`${to}`}>{`${label}`}</Link>;
}
