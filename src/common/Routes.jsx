import React from 'react';
import { Switch, Route } from "react-router-dom";
import TopAnimes from '../TopAnimes';
import TopMangas from '../TopMangas';
import TopPersona from '../TopPersona';
import TopJogos from '../TopJogos.jsx';
import TopSeries from '../TopSeries.jsx';
import Details from './Details';

export function Routes() {
  return (
    <Switch>
      <Route exact path="/top-animes">
        <TopAnimes />
      </Route>
      <Route path="/top-mangas">
        <TopMangas />
      </Route>
      <Route path="/top-persona">
        <TopPersona />
      </Route>
      <Route path="/top-jogos">
        <TopJogos />
      </Route>
      <Route path="/top-series">
        <TopSeries />
      </Route>
      <Route path="/:prefix/:id/:info" component={Details} />
    </Switch>
  )
}