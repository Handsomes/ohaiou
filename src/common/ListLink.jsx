import React from 'react';
import { Link } from "react-router-dom";

function ListLink({ prefix, list, info }) {
  const dataList = list.map((item) =>
    <ul>
      <li key={item.mal_id}>
        <Link to={`/${prefix}/${item.mal_id}/${info}`}>{item.rank && item.rank + '-'}{item.title && item.title}{item.name && item.name} </Link>
      </li>
    </ul>);

  return dataList;
}

export default ListLink;
