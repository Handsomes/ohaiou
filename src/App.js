import React from 'react';
import { BrowserRouter as Router } from "react-router-dom";
import { Button } from './common/Button.jsx';
import { Routes } from './common/Routes'

function App() {
  return (
    < Router >
      <div className="App">
        <header className="App-header">
          Ohaiou
        </header>
        <Button label='TopAnimes' to='/top-animes' />
        <Button label='TopMangas' to='/top-mangas' />
        <Button label='TopPersona' to='/top-persona' />
      </div>
      <Routes />
    </Router >
  );
}

export default App;
