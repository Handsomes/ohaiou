import React from 'react';

function OrderByScore(next, current) {
  if (next.score > current.score) {
    return -1;
  }
  if (next.score < current.score) {
    return 1;
  }
  return 0;
}
function TopJogos() {
  const jogos = [
    { titulo: 'Dark Souls 3', score: 4 },
    { titulo: 'God of War', score: 8 },
    { titulo: 'World of WarCraft', score: 10 },
    { titulo: 'Kimgdom Hearts 3', score: 7 },
    { titulo: 'League of Legends', score: 6 },
    { titulo: 'The legend of Zelda: Ocarina Of Time', score: 5 },
    { titulo: 'BloodBorne', score: 1 },
    { titulo: 'Borderlands 3', score: 2 },
    { titulo: 'OverWatch', score: 9 },
    { titulo: 'Diablo 3', score: 3 }
  ];
  const lista = (jogos.sort(OrderByScore).map((jogo) =>
    <li>{jogo.score} - {jogo.titulo}</li>
  ));

  return (
    <React.Fragment>
      <h1>Top Jogos</h1>
      <ul>
        {lista}
      </ul>
    </React.Fragment>
  )
}

export default TopJogos;