function orderByScore(next, current) {
  if (next.rank > current.rank) {
    return -1;
  }

  if (next.rank < current.rank) {
    return 1;
  }

  return 0;
}

export {
  orderByScore
};