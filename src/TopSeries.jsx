import React from 'react';

function OrderByScore(next, current) {
  if (next.score > current.score) {
    return -1;
  }
  if (next.score < current.score) {
    return 1;
  }
  return 0;
}
function TopSeries() {
  const series = [
    { titulo: 'How I Met Your Mother', score: 4 },
    { titulo: 'The Flash', score: 9 },
    { titulo: 'Supernatural', score: 7 },
    { titulo: 'The boys', score: 1 },
    { titulo: 'Game of Thrones', score: 8 },
    { titulo: 'Ash vs The Evil Dead', score: 6 },
    { titulo: 'Gothan', score: 10 },
    { titulo: 'American Gods', score: 5 },
    { titulo: '13 Reasons Why', score: 2 },
    { titulo: 'Brooklyn 99', score: 3 }
  ];
  const lista = (series.sort(OrderByScore).map((serie) =>
    <li>{serie.score} - {serie.titulo}</li>
  ));

  return (
    <React.Fragment>
      <h1>Top Series</h1>
      <ul>
        {lista}
      </ul>
    </React.Fragment>
  )
}

export default TopSeries;
