import React from 'react';
import ListLink from '../common/ListLink';
import { orderByScore } from '../utils';
import { Button } from '../common/Button';
import Search from '../common/Search.jsx';

class TopAnimes extends React.Component {
  state = {
    animes: [],
    value: '',
    isSearch: false,
    isLoading: false,
  }

  componentDidMount() {
    this.getTopAnimes();
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  handleSearch = (event) => {
    event.preventDefault();
    this.setState({ isLoading: true })
    fetch(`https://api.jikan.moe/v3/search/anime?q=${this.state.value}`)
      .then(response => response.json())
      .then(data => {
        this.setState({
          animes: data.results,
          isSearch: true,
          isLoading: false,
        })
      })
  }

  getTopAnimes = () => {
    fetch('https://api.jikan.moe/v3/top/anime/1/tv')
      .then(a => a.json())
      .then((response) => {
        this.setState({
          animes: response.top,
        });
      })
  }

  render() {
    const list = this.state.animes.sort(orderByScore);

    return (
      <React.Fragment>
        <h1>
          {
            this.state.isSearch ? 'Animes' : 'top 50 Animes'
          }
        </h1>
        <Search label='Animes' value={this.state.value} handleChange={this.handleChange} handleSearch={this.handleSearch} />
        {
          this.state.isLoading ? 'Loading...' : (
            <ListLink list={list} prefix='anime' info='details' />
          )
        }
        <Button label='Home' to='/' />
      </React.Fragment >
    )
  }
}

export default TopAnimes;
