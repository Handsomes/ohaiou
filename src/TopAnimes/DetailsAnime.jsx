import React from 'react';
import { Button } from '../common/Button';

function DetailsAnime({ details: {
  image_url,
  rank,
  url,
  title,
  title_english,
  title_japanese,
  produtora,
  episodes,
  status,
  synopsis
}
}) {

  return (
    <React.Fragment>
      <img src={image_url} alt={title} />
      <p>Rank: {rank}</p>
      <a href={url}><p>Titulo:{title}</p></a>
      <p>titulo em inglês:{title_english}</p>
      <p>Titulo Japonês: {title_japanese}</p>
      <p>Produtora: {produtora}</p>
      <p>Total de episódios: {episodes}</p>
      <p>Sinopse: {synopsis}</p>
      <p>Anime Completo: {status}</p>
      <Button label='TopAnimes' to='/top-animes' />
    </React.Fragment>
  )
}

export default DetailsAnime
