import React from 'react';
import ListLink from '../common/ListLink';
import { orderByScore } from '../utils';
import { Button } from '../common/Button';
import Search from '../common/Search.jsx';

class TopMangas extends React.Component {
  state = {
    mangas: [],
    value: '',
    isSearch: false,
    isLoading: false,
  }

  componentDidMount() {
    this.getTopMangas();
  }

  handleChange = (event) => {
    this.setState({ value: event.target.value });
  }

  handleSearch = (event) => {
    event.preventDefault();
    this.setState({ isLoading: true })
    fetch(`https://api.jikan.moe/v3/search/manga?q=${this.state.value}`)
      .then(response => response.json())
      .then(data => {
        this.setState({
          mangas: data.results,
          isSearch: true,
          isLoading: false,
        })
      })
  }

  getTopMangas = () => {
    fetch('https://api.jikan.moe/v3/top/manga/1/novels')
      .then(a => a.json())
      .then((response) => {
        this.setState({
          mangas: response.top,
        });
      })
  }

  render() {
    const list = this.state.mangas.sort(orderByScore);

    return (
      <React.Fragment>
        <h1>
          {
            this.state.isSearch ? 'Mangas' : 'top 50 Mangas'
          }
        </h1>
        <Search label='Mangas' value={this.state.value} handleChange={this.handleChange} handleSearch={this.handleSearch} />
        {
          this.state.isLoading ? 'Loading...' : (
            <ListLink list={list} prefix='manga' info='details' />
          )
        }
        <Button label='Home' to='/' />
      </React.Fragment >
    )
  }
}

export default TopMangas;
