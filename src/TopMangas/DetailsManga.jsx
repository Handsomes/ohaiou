import React from 'react';
import { Button } from '../common/Button';

function DetailsManga({ details: {
  image_url,
  rank,
  url,
  title,
  title_english,
  title_japanese,
  chapters,
  volumes,
  status,
  synopsis
}
}) {

  return (
    <React.Fragment>
      <img src={image_url} alt={title} />
      <p>Rank: {rank}</p>
      <a href={url}><p>Titulo:{title}</p></a>
      <p>titulo em inglês:{title_english}</p>
      <p>Titulo Japonês: {title_japanese}</p>
      <p>Volumes: {volumes}</p>
      <p>Produtora: {chapters}</p>
      <p>Anime Completo: {status}</p>
      <p>Sinopse:{synopsis}</p>
      <Button label='TopMangas' to='/top-mangas' />
    </React.Fragment>
  )
}

export default DetailsManga
